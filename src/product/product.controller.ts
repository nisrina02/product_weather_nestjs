import { Body, Controller, Get, Param, ParseUUIDPipe, Post, Put, Delete, HttpStatus, Query, Inject } from "@nestjs/common";
import { ProductDto } from "./dto/product.dto";
import { UpdateProductDto } from "./dto/update_product.dto";
import { ProductService } from "./product.service";

@Controller('product')
export class ProductController {
    constructor(
        private readonly productService: ProductService
    ) {}

    @Get('/')
    async getProduct(
        @Query('page') page: number
    ) {
        const { data, count } = await this.productService.getProduct(page);

        return {
            status: HttpStatus.OK,
            message: 'Success',
            data,
            count
        }
    }

    @Get('/:id')
    async getProductById(
        @Param('id', ParseUUIDPipe) id: string,
    ) {
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: await this.productService.getProductById(id)
        }
    }

    @Post('')
    async createProduct(
        @Body() input: ProductDto
    ) {
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: await this.productService.createProduct(input)
        }
    }

    @Put('/:id')
    async updateProduct(
        @Param('id', ParseUUIDPipe) id: string,
        @Body() input: UpdateProductDto
    ) {
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: await this.productService.updateProduct(id, input)
        }
    }

    @Delete('/:id')
    async deleteProduct(
        @Param('id', ParseUUIDPipe) id: string,
    ) {
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: await this.productService.deleteProduct(id)
        }
    }
}