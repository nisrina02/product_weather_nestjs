import { Test, TestingModule } from '@nestjs/testing';
import { ProductController } from './product.controller';
import { ProductService } from './product.service';

describe('ProductController', () => {
  let controller: ProductController;
  let productService: ProductService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProductController],
      providers: [ProductService],
    }).compile();

    controller = module.get<ProductController>(ProductController);
    productService = module.get<ProductService>(ProductService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('createProduct', () => {
    it('should throw an error', async () => {
        jest.spyOn(productService, 'createProduct')
            .mockImplementationOnce(() => {
                throw new BadRequestException()
            });
        try {
            const response = await controller.createProduct({
                name: 'Crew Neck Shirt',
                category: 'Clothes',
                stock: 10
            });
        } catch (err) {
            console.log(err);
        }
    });
  })
});
