import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    VersionColumn,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
} from 'typeorm';
  
@Entity({
    synchronize: true
})
export class Product {
    @PrimaryGeneratedColumn('uuid')
    id: string;
  
    @Column()
    name: string;

    @Column({
        nullable: true,
    })
    category: string;
  
    @Column({
      nullable: true,
    })
    stock: number;
  
    @CreateDateColumn({
      type: 'timestamp',
      nullable: false,
    })
    createdAt: Date;
  
    @UpdateDateColumn({
      type: 'timestamp',
      nullable: false,
    })
    updatedAt: Date;
  
    @DeleteDateColumn({
      type: 'timestamp',
      nullable: true,
    })
    deletedAt: Date;
  
    @VersionColumn()
    version: number;
  
}
  