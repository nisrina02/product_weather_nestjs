import { HttpException, HttpStatus, Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from './entities/product.entities';
import { Repository } from 'typeorm';
import * as uuid from 'uuid';
import { ProductDto } from './dto/product.dto';
import { UpdateProductDto } from './dto/update_product.dto';

@Injectable()
export class ProductService {
    constructor(
        @InjectRepository(Product)
        private productRepository: Repository<Product>
    ) {}

    async getProduct (page = 1) {
        try {
            const limit = 10;

            const product = await this.productRepository
                .createQueryBuilder('product')
                .skip(--page * limit).take(limit)
                
            const data = await product.getMany();
            const count = await product.getCount();

            if (!product) {
                throw new HttpException(
                  {
                    statusCode: HttpStatus.NOT_FOUND,
                    error: 'NOT_FOUND',
                    message: `Data product not found`,
                  },
                  HttpStatus.NOT_FOUND,
                );
              }

            return {data, count};
        } catch (err) {
            // console.log('ini error message', err.message);
            throw new HttpException(
                {
                  statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
                  message: `Internal Server Error`,
                },
                HttpStatus.INTERNAL_SERVER_ERROR,
            );
        }
    }

    async getProductById (id) {
        try {
            const product = await this.productRepository.findOne(id)

            if (!product) {
                throw new HttpException(
                  {
                    statusCode: HttpStatus.NOT_FOUND,
                    error: 'NOT_FOUND',
                    message: `Data product not found`,
                  },
                  HttpStatus.NOT_FOUND,
                );
              }

            return product;
        } catch (err) {
            // console.log('ini error message', err.message);
            throw new HttpException(
                {
                  statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
                  message: `Internal Server Error`,
                },
                HttpStatus.INTERNAL_SERVER_ERROR,
            );
        }
    }

    async createProduct (input: ProductDto) {
        try {
            const product = new Product()

            product.id = uuid.v4();
            product.name = input.name;
            product.category = input.category;
            product.stock = input.stock;

            await this.productRepository.insert(product);

            return;
        } catch (err) {
            // console.log('ini error message', err.message);
            throw new HttpException(
                {
                  statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
                  message: `Internal Server Error`,
                },
                HttpStatus.INTERNAL_SERVER_ERROR,
            );
        }
    }

    async updateProduct (id, input: UpdateProductDto) {
        const dataProduct = await this.productRepository.findOne(id)

        if (!dataProduct) {
            throw new HttpException(
                {
                  statusCode: HttpStatus.NOT_FOUND,
                  error: 'NOT_FOUND',
                  message: `Data product not found`,
                },
                HttpStatus.NOT_FOUND,
            );
        }

        try {
            const product = new Product()

            product.name = input.name;
            product.category = input.category;
            product.stock = input.stock;

            await this.productRepository.update(id, product);

            return await this.productRepository.findOne(id);
        } catch (err) {
            console.log('ini error message', err.message);
            throw new HttpException(
                {
                  statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
                  message: `Internal Server Error`,
                },
                HttpStatus.INTERNAL_SERVER_ERROR,
            );
        }
    }

    async deleteProduct (id) {
        const dataProduct = await this.productRepository.findOne(id)

        if (!dataProduct) {
            throw new HttpException(
                {
                  statusCode: HttpStatus.NOT_FOUND,
                  error: 'NOT_FOUND',
                  message: `Data product not found`,
                },
                HttpStatus.NOT_FOUND,
            );
        }

        try {
            await this.productRepository.softDelete(id);

            return;
        } catch (err) {
            // console.log('ini error message', err.message);
            throw new HttpException(
                {
                  statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
                  message: `Internal Server Error`,
                },
                HttpStatus.INTERNAL_SERVER_ERROR,
            );
        }
    }
}