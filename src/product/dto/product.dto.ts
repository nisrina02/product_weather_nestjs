import {IsNotEmpty, IsOptional} from "class-validator";

export class ProductDto {
    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    category: string;

    @IsNotEmpty()
    stock: number;
}