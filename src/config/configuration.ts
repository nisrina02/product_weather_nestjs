export default () => {
  return {
    port: parseInt(process.env.PORT, 10) || 3000,
    openWeather_API: process.env.OPENWEATHER_API || 'https://api.openweathermap.org',
    openWeather_API_KEY: process.env.OPENWEATHER_API_KEY || '3aecb64e8feb5eec375466290008c5a4',
    database: {
      client: process.env.DATABASE_CLIENT,
      host: process.env.DATABASE_HOST,
      port: parseInt(process.env.DATABASE_PORT, 10) || 3306,
      username: process.env.DATABASE_USERNAME,
      password: process.env.DATABASE_PASSWORD || '',
      name: process.env.DATABASE_NAME,
    }
  };
};
