import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import axios from 'axios';
import { ConfigService } from '@nestjs/config';
import { format } from 'date-fns'

@Injectable()
export class WeatherService {
    constructor(
        private configService: ConfigService,
    ) {}
    
    async getWeatherData (location) {
        
        try {
            let data = await axios.get(
                this.configService.get<string>('openWeather_API') +
                `/data/2.5/weather?q=${location}&appid=` + this.configService.get<string>('openWeather_API_KEY')
            ).then((res) => {
                console.log('response success get weather data')
                return res.data;
            })
            .catch((e) => {
                console.log('err get weather data', e);
                console.log('err get weather data', e?.response?.data);
                throw e;
            });
            console.log('ini data weather nya nih', data)

            const weather = {
                id: data.id,
                name: data.name,
                timezone: data.timezone,
                coord: data.coord,
                weather: data.weather[0],
                main: data.main
            }

            return weather;
        } catch (err) {
            throw new HttpException(
                {
                  statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
                  message: `Internal Server Error`,
                },
                HttpStatus.INTERNAL_SERVER_ERROR,
            );
        }
    }
}