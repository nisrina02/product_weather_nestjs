import { Controller, Get, Param, HttpStatus } from "@nestjs/common";
import { WeatherService } from "./weather.service";

@Controller('weather')
export class WeatherController {
    constructor(
        private readonly weatherService: WeatherService
    ) {}

    @Get('/:location')
    async getWeatherByLocation (
        @Param('location') location: string
    ) {
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: await this.weatherService.getWeatherData(location)
        }
    }
}