import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { WeatherController } from './weather.controller';
import { WeatherService } from './weather.service';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [TypeOrmModule.forFeature([])],
  controllers: [WeatherController],
  providers: [WeatherService, ConfigService],
  exports: [WeatherService, ConfigService],
})
export class WeatherModule {}
